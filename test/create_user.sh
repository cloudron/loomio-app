#!/bin/bash
    
set -eu

export DATABASE_URL=postgresql://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@postgresql/${CLOUDRON_POSTGRESQL_DATABASE}
source /app/data/env.sh

username=$1
password=$2

echo "user = User.new({email: '${username}', password: '${password}', password_confirmation: '${password}', name: 'Tester', email_verified: true}); user.save; exit" | /usr/local/bin/rails console
