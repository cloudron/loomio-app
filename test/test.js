#!/usr/bin/env node

/* jshint esversion: 8 */
/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.EMAIL || !process.env.PASSWORD) {
    console.log('EMAIL and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const GROUP_NAME = 'TestGroup' + Math.random().toString().slice(2, 7);
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app, cloudronName;

    const username = process.env.EMAIL;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function sleep(millis) {
        console.log(`Sleeping for ${millis/1000} seconds`);
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//input[@id="email"]'));
        await browser.findElement(By.xpath('//input[@id="email"]')).sendKeys(username);
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//button[@type="button" and contains(text(), "Continue with email")]')).click();

        // enter password
        await waitForElement(By.xpath('//input[@type="password"]'));
        await browser.findElement(By.xpath('//input[@type="password"]')).sendKeys(password);
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//button[@type="button" and contains(@class, "auth-signin-form__submit") and contains(., "Sign in")]')).click();

        await waitForElement(By.xpath('//button[@title="Notifications panel"]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated=true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(5000);

        await waitForElement(By.xpath(`//button[contains(., "Continue with ${cloudronName}") or contains(., "Continue with Cloudron")]`));
        await browser.findElement(By.xpath(`//button[contains(., "Continue with ${cloudronName}") or contains(., "Continue with Cloudron")]`)).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.sleep(4000);
        if (await browser.findElements(By.xpath(`//div[contains(@class, "auth-identity-form__new-account")]`)).then(found => !!found.length)) {
            await browser.findElement(By.xpath(`//button[contains(., "Create account")]`)).click();
            await browser.sleep(2000);
            await browser.findElement(By.xpath(`//button[contains(., "Create account")]`)).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//button[@title="Notifications panel"]'));
    }


    async function openSidebar(openProfileMenu=false) {
        await browser.sleep(1000);
        await waitForElement(By.xpath('//button[@type="button" and @aria-label="Open sidebar"]'));
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//button[@type="button" and @aria-label="Open sidebar"]')).click();

        if (openProfileMenu) {
            await waitForElement(By.xpath('//div[@class="v-list-item__icon v-list-group__header__append-icon"]'));
            await browser.sleep(1000);
            await browser.findElement(By.xpath('//div[@class="v-list-item__icon v-list-group__header__append-icon"]')).click();

            await waitForElement(By.xpath('//div[@class="v-list-item__title" and contains(text(), "Sign out")]'));
        }

        await browser.sleep(1000);
    }

    async function closeSidebar() {
        await waitForElement(By.xpath('//button[@type="button" and @aria-label="Close sidebar"]'));
        await browser.findElement(By.xpath('//button[@type="button" and @aria-label="Close sidebar"]')).click();
        await browser.sleep(1000);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);

        await openSidebar(true);
        await browser.findElement(By.xpath('//div[@class="v-list-item__title" and contains(text(), "Sign out")]')).click();
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//button[@title="Notifications panel"]'));
    }

    async function createGroup() {
        await browser.get(`https://${app.fqdn}/g/new`);
/*
        await openSidebar(false);
        await waitForElement(By.xpath('//div[text()="New group"]'));
        await browser.findElement(By.xpath('//div[text()="New group"]')).click();
        await closeSidebar();
*/
        await waitForElement(By.xpath('//input[@id="group-name"]'));
        await browser.findElement(By.xpath('//input[@id="group-name"]')).click();
        await browser.findElement(By.xpath('//input[@id="group-name"]')).sendKeys(GROUP_NAME);
        await browser.findElement(By.xpath('//button[@type="button" and contains(., "Start group")]')).click();

        await waitForElement(By.xpath(`//h1[contains(., "${GROUP_NAME}")]`));
    }

    async function checkGroup() {
        await browser.get('https://' + app.fqdn);

        await openSidebar(false);
        await waitForElement(By.xpath(`//span[text()="${GROUP_NAME}"]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no SSO
    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION} --secondary-domains CHANNELS_SERVER_DOMAIN=channels-${LOCATION},HOCUSPOCUS_SERVER_DOMAIN=hocuspocus-${LOCATION}`, EXEC_ARGS); });

    it('can create user', function() {
        getAppInfo();
        execSync(`cloudron push --app ${app.id} ./create_user.sh /app/data/create_user.sh`);
        execSync(`cloudron exec --app ${app.id} -- chmod +x /app/data/create_user.sh`);
        execSync(`cloudron exec --app ${app.id} -- /usr/bin/bash -c "/app/data/create_user.sh '${username}' '${password}'"`);
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));

    it('can get the main page', getMainPage);

    it('can create group', createGroup);

    it('check group', checkGroup);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // SSO
    it('install app (sso)', function () { execSync(`cloudron install --location ${LOCATION} --secondary-domains CHANNELS_SERVER_DOMAIN=channels-${LOCATION},HOCUSPOCUS_SERVER_DOMAIN=hocuspocus-${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login via OIDC', loginOIDC.bind(null, username, password, false));

    it('can get the main page', getMainPage);

    it('can create group', createGroup);
    it('check group', checkGroup);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('check group', checkGroup);

    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION} --secondary-domains CHANNELS_SERVER_DOMAIN=channels-${LOCATION},HOCUSPOCUS_SERVER_DOMAIN=hocuspocus-${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });

    it('can get app information', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));

    it('can get the main page', getMainPage);
    it('check group', checkGroup);

    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --secondary-domains CHANNELS_SERVER_DOMAIN=channels-${LOCATION}2,HOCUSPOCUS_SERVER_DOMAIN=hocuspocus-${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });
    it('can get app information', getAppInfo);
    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can get the main page', getMainPage);
    it('check group', checkGroup);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () { execSync(`cloudron install --appstore-id org.loomio.cloudronapp --location ${LOCATION} --secondary-domains CHANNELS_SERVER_DOMAIN=channels-${LOCATION},HOCUSPOCUS_SERVER_DOMAIN=hocuspocus-${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can get the main page', getMainPage);
    it('can create group', createGroup);
    it('check group', checkGroup);
    it('can logout', logout);

    it('can update', async function () {
        await browser.get('about:blank');
        execSync(`cloudron update --no-wait --app ${app.id}`, EXEC_ARGS);
        await sleep(20000);
        execSync(`cloudron configure --location ${LOCATION} --secondary-domains CHANNELS_SERVER_DOMAIN=channels-${LOCATION},HOCUSPOCUS_SERVER_DOMAIN=hocuspocus-${LOCATION} --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });

    it('can get app information', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('check group', checkGroup);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

