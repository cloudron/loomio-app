#!/bin/bash

set -eu

echo "=> Run hourly cron tasks"
source /app/pkg/env.sh
source /app/data/env.sh

bundle exec rake loomio:hourly_tasks
