*Collaborative Decision Making*

Loomio is a decision-making tool for collaborative organizations.

In a modern organization having everyone together in one room at the right time, with equal context to be able to make decisions is often impossible. For fast moving teams that work remotely and asynchronously it can be difficult to keep everyone on the same page about what has been decided and why.

Loomio is a tool designed for any online group to make clearer, more collaborative, better decisions together. It is perfect for:

* Remote, asynchronous teamwork. Sharing important context, making decisions and progressing work without being able to all meet at the same time or in the same place.

* Inclusive decision making. Ensure all voices that need to be heard are included in the decision making process.

* Transparency and clarity. Ensure decision making processes are clear and accessible so everyone can understand the decision, outcomes and next steps from the process.
