#!/bin/bash

set -eu

echo "==> Creating directories"
mkdir -p /run/loomio/log /run/loomio/tmp /run/loomio/.npm /run/nginx/cache /app/data/custom /app/data/storage

source /app/pkg/env.sh

if [[ ! -f /app/data/env.sh ]]; then
    echo "==> Initializing on first run"
    echo -e '# Add custom env vars here (https://github.com/loomio/loomio-deploy/blob/master/scripts/default_env)\n' >> /app/data/env.sh
    echo "export SECRET_COOKIE_TOKEN=$(openssl rand -base64 48)" >> /app/data/env.sh
    echo "export DEVISE_SECRET=$(openssl rand -base64 48)" >> /app/data/env.sh

    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        echo -e "\n# disable non-SSO logins\nexport FEATURES_DISABLE_EMAIL_LOGIN=1" >> /app/data/env.sh
    fi

    source /app/data/env.sh

    echo "==> Performing initial database setup"
    cp -f /app/code/loomio/db/schema.orig.rb /run/loomio/schema.rb
    echo "==> schema.orig.rb copied /run/loomio/schema.rb"
    rake db:schema:load
    echo "==>  db:schema:loaded "
    rm -f /run/loomio/schema.rb
    echo "==>  schema.rb deleted "
else
    source /app/data/env.sh
fi

echo "==> Changing permissions"
chown -R cloudron:cloudron /run/loomio /app/data

echo "==> Migrating Database"
gosu cloudron:cloudron rake db:migrate

echo "==> Starting Loomio"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Loomio
