FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/pkg /app/code/loomio /app/code/channel_server

# extra dependencies
RUN apt-get update && \
    apt-get install -y mupdf poppler-utils && \
    apt-get clean && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# install node v20
ARG NODE_VERSION=20.10.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH=/usr/local/node-${NODE_VERSION}/bin:$PATH

# renovate: datasource=github-releases depName=loomio/loomio versioning=semver extractVersion=^v(?<version>.+)$
ARG LOOMIO_VERSION=2.25.3

RUN curl -L https://github.com/loomio/loomio/archive/refs/tags/v${LOOMIO_VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code/loomio -f -

# Install the Loomio-specified version of Ruby
RUN echo 'gem: --no-document' >> /usr/local/etc/gemrc && \
    git clone https://github.com/rbenv/ruby-build.git && \
    ruby-build/install.sh && \
    rm -rf ruby-build && \
    (ruby-build $(cat /app/code/loomio/.ruby-version) /usr/local)

# Set up build environment
ENV BUNDLE_USER_HOME /app/code/loomio/.bundle
ENV BUNDLE_GEMFILE=/app/code/loomio/Gemfile
ENV BUNDLE_BUILD__SASSC=--disable-march-tune-native
ENV MALLOC_ARENA_MAX=2
ENV RAILS_LOG_TO_STDOUT=1
ENV RAILS_SERVE_STATIC_FILES=1
ENV RAILS_ENV=production
ENV BUNDLE_WITHOUT=development
ENV NODE_MAJOR=20

# Build app
WORKDIR /app/code/loomio
RUN bundle install
RUN bundle exec bootsnap precompile --gemfile app/ lib/

# Build UI
# https://github.com/loomio/loomio/blob/master/Dockerfile#L38
WORKDIR /app/code/loomio/vue
RUN npm install --force
RUN npm run build --verbose && \
    rm -rf node_modules

WORKDIR /app/code/loomio

RUN ln -sf /app/pkg/database.yml /app/code/loomio/config/database.yml && \
    rm -rf /app/code/loomio/storage && ln -sf /app/data/storage /app/code/loomio/storage && \
    ln -sf /app/data/custom /app/code/loomio/public/custom && \
    rm -rf /app/code/loomio/log && ln -sf /run/loomio/log /app/code/loomio/log && \
    rm -rf /app/code/loomio/tmp && ln -s /run/loomio/tmp /app/code/loomio/tmp && \
    mv /app/code/loomio/db/schema.rb /app/code/loomio/db/schema.orig.rb && \
    ln -sF /run/loomio/schema.rb /app/code/loomio/db/schema.rb

# Install channel server (websockets)
# renovate: datasource=git-refs packageName=https://github.com/loomio/loomio_channel_server branch=master
ARG LOOMIO_CHANNEL_SERVER_COMMIT=a468f4875614c35d67e4ed5f0cf45f4c49127088
RUN curl -L https://github.com/loomio/loomio_channel_server/archive/${LOOMIO_CHANNEL_SERVER_COMMIT}.tar.gz | tar zxf - --strip-components 1 -C /app/code/channel_server
WORKDIR /app/code/channel_server
RUN npm install

RUN ln -sf /run/loomio/.npm /app/code/channel_server/.npm

RUN chown -R cloudron:cloudron /app/code

# Add nginx config
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
COPY nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
COPY nginx/loomio-nginx.conf /etc/nginx/sites-enabled/loomio-nginx.conf

# Add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/loomio/supervisord.log /var/log/supervisor/supervisord.log

WORKDIR /app/code/loomio
COPY env.sh start.sh cron.sh database.yml /app/pkg/

CMD [ "/app/pkg/start.sh" ]
