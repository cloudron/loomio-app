[0.1.0]
* Initial version for Loomio

[0.2.0]
* Remove imap poll job as it does not work as expected

[0.3.0]
* Update Loomio to 2.17.2

[0.4.0]
* Update Loomio to 2.17.3
* Improvements to onboarding - we translate demo and trial group content for users.
* Update Gems and NPM modules including tiptap

[0.5.0]
* Rename env file to env.sh

[1.0.0]
* Generate secret keys properly
* Initial stable release

[1.1.0]
* Fix issues in group creation

[1.2.0]
* Update Loomio to 2.17.4
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.17.4)
* Upgrade to Rails 7 and Ruby 3.2.2
* fix for comments being on wrong thread after clicking a notification
* Ensure guests can create and announce polls as a member can, in a thread in a group
* Mark notifications as read when the user replies to a comment or reads the relevant thread
* fix email actions mark summary as read
* Threadpage, fix timestamp for "User edited the thread context"
* roboto.css - fix usage of local fonts
* Convert old document.url's to activestorage attachments

[1.2.1]
* Source `/app/data/env.sh` directly

[1.3.0]
* Update Loomio to 2.18.1
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.18.1)
* Poll Templates rewrite. We've evolved how we do Poll Templates, and it is SO much better. Here's a blog post about the new features: https://www.loomio.com/blog/2023/07/02/proposal-templates/
* If a user changes their position on a poll/proposal within a thread, we create a new stance. This fixes the issue that if people reply to your vote reason, then you change your vote the comments don't make sense. Now it feels like the new vote is a new comment.
* We fixed a major bug with Mark as Read, and it's crazy how long this was not reported for.

[1.3.1]
* Update Loomio to 2.18.2
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.18.2)
* If you change your vote, and you change from say, disagree to accept, then a new stance will be created in the thread. This is so that any existing comments on your vote still make sense.

[1.3.2]
* Update Loomio to 2.18.3
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.18.3)
* Fix bug introduced 2 weeks ago where comments would appear on previous thread
* Restore feature to rename and delete tags within a group, following a big upgrade to the tag system which allows anyone to add a new tag to a poll or thread.

[1.4.0]
* Update Loomio to 2.19.0
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.19.0)
* If you search for something that exists, it will find it!
* You can sort the results by 'newest', 'oldest' or 'best match'
* You can include the author name with your search terms to narrow the scope of the results
* You can combine the search with a tag to narrow the results down
* You can filter by type "Thread", "Comment", "Poll", "Vote" etc

[1.5.0]
* Update Loomio to 2.20.0
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.20.0)
* Include "visible to parent members" content when searching
* fix bug where search modal would not open sometimes
* ignore google docs sign-in links in link-previews
* Move group CSV export to a background job that emails you when ready
* Change 'add option' ui from a name field, to a button that opens a modal
* adjust default order of `poll_templates`
* update blacklisted passwords. list compiled from top 100K passwords

[1.5.1]
* Update Loomio to 2.20.1
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.20.1)
* Change memberships.archived_at to revoked_at.

[1.5.2]
* Update Loomio to 2.21.0
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.21.0)

[1.5.3]
* Update Loomio to 2.21.1
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.21.1)
* allow mentioning unverified users
* fix "Make a copy" of thread
* dont fetch discussion_templates/null from /d/new
* Enable creation of a user account with a pending_group invitation
* clone category of group

[1.5.4]
* Update Loomio to 2.21.2
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.21.2)
* "554 files changed. That's me converting every piece of Coffeescript into JS."
* Ensure we only show "notify everyone when poll starts" on new poll form
* fix bug where you could not edit outcome review date
* You can now provide template text for a outcome statement as part of the proposal template

[1.5.5]
* Update Loomio to 2.21.3
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.21.3)
* Thread page performance is greatly improved. Less jumping when opening a thread, faster loading.
* Show tooltip with option meaning in poll results table
* fix for mentioning users in group descriptions
* only create a new stance event in a discussion if there are replies
* Fix issue #9818 "needs vote" filter does not update pagination
* Ensure we pretranslate demo group in all supported locales
* fix issue downloading user avatar when using single sign on
* fix tasks page for outcome task
* better detection of google docs sign in title
* Fix bug on discussion form when using a template
* Don't show replies or reactions or allow comments if a polls results are hidden
* Do not show poll results in thread nav sidebar if results are hidden

[1.5.6]
* Update Loomio to 2.21.4
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.21.4)
* We've included the new "email to group" feature. This means as a member of a group, you can send or forward an email to your group email address and it will become a new thread in the group.
* We've also done a lot of work on tidying the CSS, fixing bugs and improving load times significantly. Using the new vite bundler we now only load the parts of the app that are necessary on first load.
* Link previews have been tamed a little, so they're not so big, and we don't show repeats if the link already has a preview in the thread.

[1.5.7]
* Update Loomio to 2.21.5
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.21.5)
* add width: auto for formatted-text img
* add OAUTH_LOGIN_PROVIDER_NAME and SAML_LOGIN_PROVIDER_NAME vars (allowing you to give a name to your login provider button)
* avoid sending empty catch up emails
* fix discussion_edited event so it shows the edit time correctly
* fix display of poll results for some cases in dot vote poll
* Update ReceviedEmailService#delete_old_emails to treat all emails the same
* fix an issue where voter anonymity could be revealed
* fix a problem where the poll results filter would reveal what someone voted for
* When a thread has an anonymous poll, hide the announcement viewed information

[1.6.0]
* Update Loomio to 2.22.0
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.22.0)
* Membership records are no longer deleted when you remove a member. Instead we mark the membership as revoked, so we can see who revoked the membership and when
* Mentioning a user with @mention is much (approximately 10x) faster now. Thank goodness, eh.
* We fixed quotes when writing in markdown
* Fix handling of attachments when using local disk service
* Blank thread template will notify the group by default

[1.7.0]
* Add Cloudron OIDC integration

[1.7.1]
* Fix cron job to have all required environment variables available

[1.7.2]
* Update Loomio to 2.22.1
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.22.1)
* A number of important bug fixes related to handling membership records
* Breaking change: remove api/b1 interface. Please upgrade to api/b2 if you use the Loomio API to create threads or polls
* Remove new thread button from sidebar. You need to start a thread from the group page, or from the private threads page, so that you choose a template when starting a thread
* Fix permissions for close/reopen thread

[1.7.3]
* Update Loomio to 2.22.2
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.22.2)
* Fix: Use the right icon for abstain (thanks @ludat)
* Fix Address low risk RCE vulnerability when importing group data via the admin panel
* Feature: User edit profile page will allow you to manually select your time zone
* Improvement: only show public groups on user profile page
* Fix UX: do not set notification recipients from template when editing a discussion

[1.7.4]
* Update Loomio to 2.24.0
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.24.0)
* fix pie graph not rendering on app load in some cases
* fix pie graph not showing in emails
* don't mark as read if not logged in
* include attachments in stances when viewing the poll page
* Check user passwords against the 'haveibeenpwned' API
* Improvements to delete and redact user
* Changes to how we handle subscriptions
* bring back new thread button as opt in feature
* Add support for Shift+Enter to insert linebreak
* Rename invite-only threads to direct threads
* Fix for edgecase to allow emails to be routed when member alias exists
* simplify the logic about announcing a poll
* dont lazyload users, rather fetch all users for a poll when we show
* Enable admins to revoke memberships. Thanks @anarute

[1.8.0]
* Update Loomio to 2.25.0
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.25.0)

[1.8.1]
* Update Loomio to 2.25.1
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.25.1)

[1.8.2]
* Update websocket integration

[1.8.3]
* Update Loomio to 2.25.2
* [Full changelog](https://github.com/loomio/loomio/releases/tag/v2.25.2)
* If you run a Loomio server - I'd like to request support from the community - We're looking to build a collection of testimonials to help with marketing. Please email me with a short description of what you value about Loomio, and how your group uses it. I'd really appreciate it.
* feature: score and `dot_vote` forms: allow user to manually enter the exact number manually
* fix: poll-common-form add setTimeout to ensure the value in the timeslot comobox is observed
* fix: reset discussion form after save (bug related to auto save draft)
* change: dont email group admins when an unrecognised sender emails group email address
* feature: Add a reset button to the edit comment form - so if you make changes you can undo them
* fix: Ensure files from "parent members can see discussions" subgroups show up in parent group files tab
* improvement: Add "want to sign in with password next time" to auth form. make it more obvious how to set a password when signing in
* fix: ensure that when discussion or poll is moved, we create stances for members of the destination group
* fix: fix blockquote email styles - the grey was intense

[1.9.0]
* **Important:** Configure a new subdomain for hocuspocus in the Location view after updating the app.

[1.9.1]
* Update loomio to 2.25.3
* [Full Changelog](https://github.com/loomio/loomio/releases/tag/v2.25.3)
* [fix bug with discard poll](https://github.com/loomio/loomio/commit/8f8301f6f0135507cd0e5704b6f64504bdd1230f)
* [fix bug where discussions list appears empty when all discussions are pinned](https://github.com/loomio/loomio/commit/35974e73bf75611787c4b76af91596a48d7913a6)
* [support custom logo in boot screen](https://github.com/loomio/loomio/commit/259a7deec43e16be75a2e40335d452ed46b660d3)
* [if links in user generated content point to same host, don't open in new tab](https://github.com/loomio/loomio/commit/67b4e9a162f26e0ac5e9433959837e74de69fdd3)
* [fix admin/update admin/delete for forward email rules](https://github.com/loomio/loomio/commit/c6757381b5d9fdc90872e6ca3184486e51c83abc)
* [add ENV FEATURES_DEFAULT_PATH to redirect users to a group rather than dashboard](https://github.com/loomio/loomio/commit/52f16617dc0e150b04202678e60f87166f3db4a2)
* [disable creating direct threads without belonging to a group](https://github.com/loomio/loomio/commit/656530e8a710001b88cd2064aba12b4d8988f37c)

[1.9.2]
* CLOUDRON_OIDC_PROVIDER_NAME implemented, checklist added, tests fixed

