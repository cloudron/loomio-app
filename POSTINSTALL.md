<nosso>
Depending on your setup, you can customize Loomio to disable public user registration (invite only)
and disable creation of groups.
</nosso>

<sso>
External login is disabled. To enable external login, change `FEATURES_DISABLE_EMAIL_LOGIN` in `/app/data/env.sh`.
</sso>
